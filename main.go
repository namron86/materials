package main

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"os"

	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/aws/aws-xray-sdk-go/xraylog"
)

type material struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Cost        int    `json:"cost"`
}

func listMaterials() []material {

	materials := []material{
		material{"adamantine", "foo", 1000},
	}

	return materials
}

func describeMaterial(name string) (material *material, err error) {

	materials := listMaterials()

	for _, material := range materials {
		if material.Name == name {
			return &material, nil
		}
	}

	return nil, errors.New("material could not be found")
}

func handleGetMaterials(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Path[1:]

	if name != "" {
		material, err := describeMaterial(name)
		if err != nil {
			w.WriteHeader(404)
			return
		}

		js, err := json.Marshal(*material)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(200)
		w.Write(js)
		return
	}

	js, err := json.Marshal(listMaterials())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(200)
	w.Write(js)
}

func getMaterial(w http.ResponseWriter, r *http.Request) {

}

func main() {
	xray.SetLogger(xraylog.NewDefaultLogger(os.Stderr, xraylog.LogLevelError))
	http.Handle("/", xray.Handler(xray.NewFixedSegmentNamer("materials"), http.HandlerFunc(handleGetMaterials)))
	log.Fatal(http.ListenAndServe(":5000", nil))
}
