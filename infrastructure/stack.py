from aws_cdk import (
    aws_ec2 as ec2,
    aws_ecs as ecs,
    aws_ecr as ecr,
    aws_iam as iam,
    aws_appmesh as appmesh,
    aws_ecs_patterns as ecs_patterns,
    aws_servicediscovery as servicediscovery,
    core,
)

materials_image = "014206379538.dkr.ecr.us-west-2.amazonaws.com/materials:latest"


class MaterialsServiceStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, *kwargs)

        deploy_env = "qa101"
        cloud_map_namespace = f"{deploy_env}.private.kount.com"

        ### The VPC and Mesh would normally be defined outside this stack ###
        vpc = ec2.Vpc(
            self, "MainVpc",
            max_azs=2
        )
        
        cluster = ecs.Cluster(
            self, 'MainCluster',
            vpc=vpc
        )

        cloud_map_namespace = servicediscovery.CfnPrivateDnsNamespace(
            self, "CloudMapPrivateDnsNamespace",
            name=cloud_map_namespace,
            vpc=vpc.vpc_id,
            
        )

        mesh = appmesh.CfnMesh(
            self, "AppMesh",
            mesh_name=deploy_env,
            spec={
                "egressFilter": {
                    "type": "ALLOW_ALL"
                }
            }
        )


        ######################################################################

        # Start app specific stack resource definitions
        approle = "materials"
        virtual_node_name = f"{approle}-virtual-node"
        virtual_service_name = f"{approle}-virtual-service"
        cloud_map_service_name = approle

        cloud_map_service = servicediscovery.CfnService(
            self, "CloudMapService",
            namespace_id=cloud_map_namespace.ref,
            name=cloud_map_service_name,
            dns_config={
                "dnsRecords": [
                    {
                        "ttl": 30,
                        "type": "A"
                    }
                ],
                "namespaceId": cloud_map_namespace.ref,
                "routingPolicy": "WEIGHTED",
            }
        )

        virtual_node = appmesh.CfnVirtualNode(
            self, "AppMeshVirtualNode",
            mesh_name=mesh.mesh_name,
            virtual_node_name=virtual_node_name,
            spec={
                "listeners": [
                    {
                        "portMapping": {
                            "port": 5000,
                            "protocol": "tcp"
                        }
                    }
                ],
                "serviceDiscovery": {
                    "awsCloudMap": {
                        "namespaceName": deploy_env,
                        "serviceName": cloud_map_service.name
                    }
                }
            }
        )
        virtual_node.add_depends_on(mesh)

        virtual_service = appmesh.CfnVirtualService(
            self, "AppMeshVirtualService",
            mesh_name=deploy_env,
            virtual_service_name=virtual_service_name,
            spec={
                "provider": {
                    "virtualNode": {
                        "virtualNodeName": virtual_node_name
                    }
                }
            },
        )
        virtual_service.add_depends_on(mesh)
        virtual_service.add_depends_on(virtual_node)

        sg = ec2.SecurityGroup(self, "MrnServiceSecurityGroup", vpc=vpc, allow_all_outbound=True)
        sg.add_ingress_rule(ec2.Peer.any_ipv4(), ec2.Port.tcp(5000))

        task_execution_role = iam.Role(
            self, "EcsTaskExeuctionRole",
            managed_policies=[
                iam.ManagedPolicy.from_aws_managed_policy_name("AmazonEC2ContainerRegistryReadOnly"),
                iam.ManagedPolicy.from_aws_managed_policy_name("CloudWatchLogsFullAccess"),
            ],
            assumed_by=iam.ServicePrincipal("ecs-tasks.amazonaws.com")
        )

        task_role = iam.Role(
            self, "EcsTaskRole",
            managed_policies=[                
                iam.ManagedPolicy.from_aws_managed_policy_name("AWSXRayDaemonWriteAccess"),
                iam.ManagedPolicy.from_aws_managed_policy_name("AWSCloudMapRegisterInstanceAccess"),
                iam.ManagedPolicy.from_aws_managed_policy_name("AWSAppMeshEnvoyAccess"),
            ],
            assumed_by=iam.ServicePrincipal("ecs-tasks.amazonaws.com")
        )

        log_configuration = {
            "logDriver": "awslogs",
            "options": {
                "awslogs-group": f"/ecs/{approle}",
                "awslogs-region": "us-west-2",
                "awslogs-stream-prefix": "ecs",
            }
        }

        materials_container = {
            "name": approle,
            "image": materials_image,
            "logConfiguration": log_configuration,
            "portMappings": [{
                "containerPort": 5000,
                "hostPort": 5000,
                "protocol": "tcp"
            }],
            "essential": True,
            "memory": 512,
            "cpu": 256,
            "dependsOn": [{
                "containerName": "envoy",
                "condition": "HEALTHY"
            }],
        }

        envoy_container = {
            "name": "envoy",
            "image": "111345817488.dkr.ecr.us-west-2.amazonaws.com/aws-appmesh-envoy:v1.9.1.0-prod",
            "logConfiguration": log_configuration,
            "essential": True,
            "environment": [
                {
                    "name": "APPMESH_VIRTUAL_NODE_NAME",
                    "value": f"mesh/{mesh.mesh_name}/virtualNode/{virtual_node.virtual_node_name}"
                },
                {
                    "name": "ENABLE_ENVOY_XRAY_TRACING",
                    "value": "1"
                 }
            ],
            "healthCheck": {
                "command": [
                    "CMD-SHELL",
                    "curl -s http://localhost:9901/server_info | grep state | grep -q LIVE"
                ],
                "startPeriod": 10,
                "interval": 5,
                "timeout": 2,
                "retries": 3
            },
            "user": "1337"
        }

        xray_daemon_container = {
            "name" : "xray-daemon",
            "image" : "amazon/aws-xray-daemon",
            "logConfiguration": log_configuration,

            "user" : "1337",
            "essential" : True,
            "cpu" : 32,
            "memoryReservation" : 256,
            "portMappings" : [
               {
                  "containerPort" : 2000,
                  "protocol" : "udp"
               }
            ]
         }

        proxy_configuration = {
            "containerName": "envoy",
            "type": "APPMESH",
            "proxyConfigurationProperties": [
                {
                    "name": "IgnoredUID",
                    "value": "1337"
                },
                {
                    "name": "ProxyIngressPort",
                    "value": "15000"
                },
                {
                    "name": "ProxyEgressPort",
                    "value": "15001"
                },
                {
                    "name": "AppPorts",
                    "value": "5000"
                },
                {
                    "name": "EgressIgnoredIPs",
                    "value": "169.254.170.2,169.254.169.254"
                }
            ]
        }

        private_subnet_ids = list()
        for s in vpc.private_subnets:
            private_subnet_ids.append(s.subnet_id)

        awsvpc_configuration = ecs.CfnService.AwsVpcConfigurationProperty(subnets=private_subnet_ids, security_groups=[sg.security_group_id])
        network_configuration = ecs.CfnService.NetworkConfigurationProperty(awsvpc_configuration=awsvpc_configuration)

        task = ecs.CfnTaskDefinition(
            self, "EcsTask",
            execution_role_arn=task_execution_role.role_arn,
            task_role_arn=task_role.role_arn,
            container_definitions=[materials_container, envoy_container, xray_daemon_container],
            memory="1024",
            cpu="512",
            requires_compatibilities=["FARGATE"],
            network_mode="awsvpc",
            proxy_configuration=proxy_configuration,
        )

        ecs.CfnService(
            self, "EcsService",
            task_definition=task.ref,
            cluster=cluster.cluster_arn,
            desired_count=3,
            launch_type="FARGATE",
            network_configuration=network_configuration,
            service_registries=[
                {
                    "registryArn": cloud_map_service.attr_arn
                }
            ]
        )


app = core.App()
MaterialsServiceStack(app, "MaterialsServiceStack")
app.synth()
