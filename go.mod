module gitlab.com/namron86/materials

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3 // indirect
	github.com/aws/aws-sdk-go v1.22.3 // indirect
	github.com/aws/aws-xray-sdk-go v1.0.0-rc.13
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
)
