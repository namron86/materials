ECR_TAG=014206379538.dkr.ecr.us-west-2.amazonaws.com/materials:latest

build_image:
	docker build -t materials .

deploy_image: build_image
	docker tag materials:latest ${ECR_TAG}
	eval $$(aws ecr get-login --no-include-email)
	docker push ${ECR_TAG}
