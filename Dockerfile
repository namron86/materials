FROM golang:1.12.7-alpine3.10 AS build
RUN apk add --update git
WORKDIR /home
COPY . .
RUN go mod download
RUN go build

FROM alpine:3.10
COPY --from=build /home/materials materials
EXPOSE 5000
CMD ["./materials"]